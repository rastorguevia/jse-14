package ru.rastorguev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.entity.ComparableEntity;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.ProjectPOJO;
import ru.rastorguev.tm.entity.TaskPOJO;
import ru.rastorguev.tm.entity.UserPOJO;
import ru.rastorguev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.*;


@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements ComparableEntity, Serializable {

    @NotNull
    private String userId;

    @NotNull
    private String projectId = "";

    @NotNull
    private String projectName;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long creationDate = System.currentTimeMillis();

    public Task(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        this.projectId = projectId;
    }

    @Nullable
    public static TaskPOJO toTaskPojo(
            @NotNull final ServiceLocator serviceLocator,
            @Nullable final Task task
    ) throws Exception {
        if (task == null) return null;
        @NotNull final TaskPOJO taskPojo = new TaskPOJO();
        @Nullable final UserPOJO user = serviceLocator.getUserService().findOne(task.getUserId());
        if (user == null) return null;
        @Nullable final ProjectPOJO project = serviceLocator.getProjectService().findOne(task.getProjectId());
        if (project == null) return null;
        taskPojo.setId(task.getId());
        taskPojo.setUser(user);
        taskPojo.setProject(project);
        taskPojo.setName(task.getName());
        taskPojo.setDescription(task.getDescription());
        taskPojo.setStartDate(task.getStartDate());
        taskPojo.setEndDate(task.getEndDate());
        taskPojo.setStatus(task.getStatus());
        taskPojo.setCreationDate(task.getCreationDate());
        return taskPojo;
    }

}