package ru.rastorguev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.entity.ComparableEntity;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.ProjectPOJO;
import ru.rastorguev.tm.entity.TaskPOJO;
import ru.rastorguev.tm.entity.UserPOJO;
import ru.rastorguev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static ru.rastorguev.tm.util.DateUtil.*;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements ComparableEntity, Serializable {

    @NotNull
    private String userId;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long creationDate = System.currentTimeMillis();

    @Nullable
    public static ProjectPOJO toProjectPojo(
            @NotNull final ServiceLocator serviceLocator,
            @Nullable final Project project
            ) throws Exception {
        if (project == null) return null;
        @NotNull final ProjectPOJO projectPojo = new ProjectPOJO();
        @NotNull final List<TaskPOJO> taskList = serviceLocator.getTaskService().taskListByProjectId(project.getId());
        @Nullable final UserPOJO user = serviceLocator.getUserService().findOne(project.getUserId());
        if (user == null) return null;
        projectPojo.setId(project.getId());
        projectPojo.setUser(user);
        projectPojo.setTasks(taskList);
        projectPojo.setName(project.getName());
        projectPojo.setDescription(project.getDescription());
        projectPojo.setStartDate(project.getStartDate());
        projectPojo.setEndDate(project.getEndDate());
        projectPojo.setStatus(project.getStatus());
        projectPojo.setCreationDate(project.getCreationDate());
        return projectPojo;
    }

}