package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.SessionPOJO;

public interface ISessionRepository extends IRepository<SessionPOJO> {

}
