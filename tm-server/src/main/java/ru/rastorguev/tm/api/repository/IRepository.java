package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.AbstractPOJO;

import java.util.List;

public interface IRepository <E extends AbstractPOJO> {

    List<E> findAll();

    E findOne(final String id);

    void persist(final E e);

    void merge(final E e);

    void remove(final String id) throws Exception;

    void removeAll();

}
