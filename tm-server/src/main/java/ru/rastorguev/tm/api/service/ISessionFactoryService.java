package ru.rastorguev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public interface ISessionFactoryService {

    SqlSessionFactory getSqlSessionFactory() throws Exception;

    SqlSession getSession() throws Exception;

}
