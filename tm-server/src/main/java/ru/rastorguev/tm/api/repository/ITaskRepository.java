package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.TaskPOJO;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskPOJO> {

    List<TaskPOJO> taskListByProjectId(final String projectId);

    void removeTaskListByProjectId(final String projectId);

    List<TaskPOJO> taskListByUserId(final String userId);

    List<TaskPOJO> filteredTaskListByUserIdAndInput(final String userId, final String input);

    List<TaskPOJO> taskListByProjectIdSorted(final String projectId, final String sortType);

}
