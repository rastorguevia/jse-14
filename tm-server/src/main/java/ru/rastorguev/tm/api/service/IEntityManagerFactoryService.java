package ru.rastorguev.tm.api.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IEntityManagerFactoryService {

    EntityManagerFactory factory() throws Exception;

    EntityManager getEntityManager() throws Exception;

}
