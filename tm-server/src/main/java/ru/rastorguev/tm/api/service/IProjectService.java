package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.dto.Project;
import ru.rastorguev.tm.entity.ProjectPOJO;

import java.util.List;

public interface IProjectService extends IService<ProjectPOJO> {

    List<ProjectPOJO> findAllByUserId(final String userId) throws Exception;

    List<ProjectPOJO> findAllByUserIdSorted(final String userId, final String sortType) throws Exception;

    void removeAllByUserId(final String userId) throws Exception;

    String getProjectIdByNumberForUser(final int number, final String userId) throws Exception;

    List<ProjectPOJO> findProjectsByInputAndUserId(final String input, final String userId) throws Exception;

    void loadFromDto(final List<ProjectPOJO> projectList) throws Exception;

}
