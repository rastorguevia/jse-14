package ru.rastorguev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.dto.User;
import ru.rastorguev.tm.entity.UserPOJO;
import ru.rastorguev.tm.enumerated.Role;

import java.util.List;

public interface IUserService extends IService<UserPOJO> {

    UserPOJO findByLogin(final String login) throws Exception;

    boolean isExistByLogin(final String login) throws Exception;

    UserPOJO createUser(final String login, final String password) throws Exception;

    UserPOJO createAdmin(final String login, final String password) throws Exception;

    void updateUserLogin(final String userId, final String login) throws Exception;

    void updateUserPassword(final String userId, final String password, final String old) throws Exception;

    void loadFromDto(final List<UserPOJO> userList) throws Exception;

    Role getUserRole(@Nullable final String userId) throws Exception;

}
