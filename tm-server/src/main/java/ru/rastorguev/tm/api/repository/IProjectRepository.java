package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.ProjectPOJO;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectPOJO> {

    List<ProjectPOJO> findAllByUserId(final String userId);

    void removeAllByUserId(final String userId);

    List<ProjectPOJO> findAllByUserIdSorted(final String userId, final String sortType);

    List<ProjectPOJO> findProjectsByInputAndUserId(final String input, final String userId);

}
