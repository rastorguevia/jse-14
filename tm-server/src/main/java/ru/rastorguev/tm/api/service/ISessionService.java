package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.dto.Session;
import ru.rastorguev.tm.entity.SessionPOJO;
import ru.rastorguev.tm.enumerated.Role;

public interface ISessionService extends IService<SessionPOJO> {

    String createNewSession(final String login, final String password) throws Exception;

    boolean isContains(final Session session) throws Exception;

    void validate(final Session session) throws Exception;

    void validateSessionAndRole(final Session session,final Role role) throws Exception;

}
