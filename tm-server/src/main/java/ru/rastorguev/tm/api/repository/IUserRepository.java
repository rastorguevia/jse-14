package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.UserPOJO;

public interface IUserRepository extends IRepository<UserPOJO> {

    UserPOJO findByLogin(final String login);

    boolean isExistByLogin(final String login);

}
