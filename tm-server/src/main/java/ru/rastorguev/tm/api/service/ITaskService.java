package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.TaskPOJO;

import java.util.List;

public interface ITaskService extends IService<TaskPOJO> {

    void removeTaskListByProjectId(final String projectId) throws Exception;

    String getTaskIdByNumberAndProjectId(final int number, String projectId) throws Exception;

    List<TaskPOJO> taskListByUserId(final String userId) throws Exception;

    List<TaskPOJO> taskListByProjectIdSorted(final String projectId, final String sortType) throws Exception;

    List<TaskPOJO> filteredTaskListByUserIdAndInput(final String userId, final String input) throws Exception;

    List<TaskPOJO> taskListByProjectId(final String projectId) throws Exception;

    void loadFromDto(final List<TaskPOJO> taskList) throws Exception;

}
