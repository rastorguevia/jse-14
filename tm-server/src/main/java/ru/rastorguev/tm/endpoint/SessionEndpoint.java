package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.dto.Session;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.util.EncryptionUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.ISessionEndpoint")
public final class SessionEndpoint implements ISessionEndpoint {

    private ServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public String createNewSession(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
       return serviceLocator.getSessionService().createNewSession(login, password);
    }

    @Override
    @WebMethod
    public void removeSession(@WebParam(name = "token") @Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().remove(session.getId());
    }

}
