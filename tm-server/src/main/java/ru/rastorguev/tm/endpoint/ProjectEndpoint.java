package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.dto.Project;
import ru.rastorguev.tm.dto.Session;
import ru.rastorguev.tm.entity.ProjectPOJO;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.util.EncryptionUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Project createProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        if (project == null) throw new AccessDeniedException("Access denied. Project does not exist.");
        serviceLocator.getSessionService().validate(session);
        @Nullable final ProjectPOJO projectPojo =
                serviceLocator.getProjectService().persist(Project.toProjectPojo(serviceLocator, project));
        @Nullable final Project finalProject = ProjectPOJO.toProject(projectPojo);
        return finalProject;
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @Nullable final ProjectPOJO projectPojo =
                serviceLocator.getProjectService().merge(Project.toProjectPojo(serviceLocator, project));
        @Nullable final Project finalProject = ProjectPOJO.toProject(projectPojo);
        return finalProject;
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(projectId);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUser(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAllByUserId(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjectsForUser(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @NotNull final List<ProjectPOJO> projectPojoList =
                serviceLocator.getProjectService().findAllByUserId(session.getUserId());
        @NotNull final List<Project> projects = new LinkedList<>();
        for (ProjectPOJO projectPojo : projectPojoList) {
            projects.add(ProjectPOJO.toProject(projectPojo));
        }
        return projects;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjectsForUserSorted(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "sortType") @Nullable final String sortType
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @NotNull final List<ProjectPOJO> projectPojoList =
                serviceLocator.getProjectService().findAllByUserIdSorted(session.getUserId(), sortType);
        @NotNull final List<Project> projects = new LinkedList<>();
        for (ProjectPOJO projectPojo : projectPojoList) {
            projects.add(ProjectPOJO.toProject(projectPojo));
        }
        return projects;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findProjectsByUserIdAndInput(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "input") @Nullable final String input
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);

        @NotNull final List<ProjectPOJO> projectPojoList =
                serviceLocator.getProjectService().findProjectsByInputAndUserId(input, session.getUserId());
        @NotNull final List<Project> projects = new LinkedList<>();
        for (ProjectPOJO projectPojo : projectPojoList) {
            projects.add(ProjectPOJO.toProject(projectPojo));
        }
        return projects;
    }

    @Nullable
    @Override
    @WebMethod
    public String projectIdByNumber(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "number") final int number
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().getProjectIdByNumberForUser(number, session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getProjectService().removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @Nullable final ProjectPOJO projectPojo = serviceLocator.getProjectService().findOne(projectId);
        @Nullable final Project project = ProjectPOJO.toProject(projectPojo);
        return project;
    }

}
