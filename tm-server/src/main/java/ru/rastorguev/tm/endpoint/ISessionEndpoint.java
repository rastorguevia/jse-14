package ru.rastorguev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    String createNewSession(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) throws Exception;

    @WebMethod
    void removeSession(@WebParam(name = "token") final String token) throws Exception;

}
