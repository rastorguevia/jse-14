package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.dto.Session;
import ru.rastorguev.tm.dto.Task;
import ru.rastorguev.tm.entity.TaskPOJO;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.util.EncryptionUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private ServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Task createTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @Nullable final TaskPOJO taskPojo = serviceLocator.getTaskService().persist(Task.toTaskPojo(serviceLocator, task));
        @Nullable final Task finalTask = TaskPOJO.toTask(taskPojo);
        return finalTask;
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @Nullable final TaskPOJO taskPojo = serviceLocator.getTaskService().merge(Task.toTaskPojo(serviceLocator, task));
        @Nullable final Task finalTask = TaskPOJO.toTask(taskPojo);
        return finalTask;
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(taskId);
    }

    @Override
    @WebMethod
    public void removeTaskListByProjectId(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeTaskListByProjectId(projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public String taskIdByNumberAndProjectId(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "number") final int number,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getTaskIdByNumberAndProjectId(number, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> taskListByUserId(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @NotNull final List<TaskPOJO> taskPojoList = serviceLocator.getTaskService().taskListByUserId(userId);
        @NotNull final List<Task> taskList = new LinkedList<>();
        for (TaskPOJO taskPojo : taskPojoList) {
            taskList.add(TaskPOJO.toTask(taskPojo));
        }
        return taskList;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> filteredTaskListByInput(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "input") @NotNull final String input
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @NotNull final List<TaskPOJO> taskPojoList =
                serviceLocator.getTaskService().filteredTaskListByUserIdAndInput(session.getUserId(), input);
        @NotNull final List<Task> taskList = new LinkedList<>();
        for (TaskPOJO taskPojo : taskPojoList) {
            taskList.add(TaskPOJO.toTask(taskPojo));
        }
        return taskList;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByProjectId(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @NotNull final List<TaskPOJO> taskPojoList = serviceLocator.getTaskService().taskListByProjectId(projectId);
        @NotNull final List<Task> taskList = new LinkedList<>();
        for (TaskPOJO taskPojo : taskPojoList) {
            taskList.add(TaskPOJO.toTask(taskPojo));
        }
        return taskList;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByProjectIdSorted(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "sortType") @NotNull final String sortType
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @NotNull final List<TaskPOJO> taskPojoList =
                serviceLocator.getTaskService().taskListByProjectIdSorted(projectId, sortType);
        @NotNull final List<Task> taskList = new LinkedList<>();
        for (TaskPOJO taskPojo : taskPojoList) {
            taskList.add(TaskPOJO.toTask(taskPojo));
        }
        return taskList;
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    @WebMethod
    public Task findTask (
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied. Token is empty.");
        @Nullable final Session session = EncryptionUtil.decrypt(token);
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        @Nullable final TaskPOJO taskPojo = serviceLocator.getTaskService().findOne(taskId);
        @Nullable final Task task = TaskPOJO.toTask(taskPojo);
        return task;
    }

}
