package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.service.IEntityManagerFactoryService;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.TaskPOJO;
import ru.rastorguev.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public final class TaskService extends AbstractService<TaskPOJO> implements ITaskService {

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactory;

    @NotNull
    @Override
    public List<TaskPOJO> findAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> taskList = taskRepository.findAll();
        entityManager.close();
        return taskList;
    }

    @Nullable
    @Override
    public TaskPOJO findOne(@Nullable final String taskId) throws Exception {
        if (taskId == null || taskId.isEmpty()) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final TaskPOJO task = taskRepository.findOne(taskId);
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public TaskPOJO persist(@Nullable final TaskPOJO task) throws Exception {
        if (task == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            taskRepository.persist(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskPOJO merge(@Nullable final TaskPOJO task) throws Exception {
        if (task == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            taskRepository.merge(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String taskId) throws Exception {
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            taskRepository.remove(taskId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            taskRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeTaskListByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            taskRepository.removeTaskListByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskPOJO> taskListByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> taskList = taskRepository.taskListByProjectId(projectId);
        entityManager.close();
        return taskList;
    }

    @Nullable
    @Override
    public String getTaskIdByNumberAndProjectId(final int number, @Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull final List<TaskPOJO> filteredListOfTasks = taskListByProjectId(projectId);
        if (filteredListOfTasks == null) return null;
        return filteredListOfTasks.get(number - 1).getId();
    }

    @NotNull
    @Override
    public List<TaskPOJO> taskListByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> taskList = taskRepository.taskListByUserId(userId);
        entityManager.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskPOJO> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (input == null || input.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> taskList = taskRepository.filteredTaskListByUserIdAndInput(userId, input);
        entityManager.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskPOJO> taskListByProjectIdSorted(@Nullable final String projectId, @Nullable final String sortType) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> taskList = taskRepository.taskListByProjectIdSorted(projectId, sortType);
        entityManager.close();
        return taskList;
    }

    @Override
    public void loadFromDto(@Nullable final List<TaskPOJO> taskList) throws Exception {
        if (taskList == null) return;
        for (final TaskPOJO task : taskList){
            @Nullable TaskPOJO t = findOne(task.getId());
            if (t == null) persist(task);
            else merge(task);
        }
    }

}