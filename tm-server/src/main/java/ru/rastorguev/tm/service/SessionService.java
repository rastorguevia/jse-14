package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.IEntityManagerFactoryService;
import ru.rastorguev.tm.api.service.ISessionService;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.dto.Session;
import ru.rastorguev.tm.entity.SessionPOJO;
import ru.rastorguev.tm.entity.UserPOJO;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.repository.SessionRepository;
import ru.rastorguev.tm.repository.UserRepository;
import ru.rastorguev.tm.util.EncryptionUtil;
import ru.rastorguev.tm.util.MD5Util;
import ru.rastorguev.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class SessionService extends AbstractService<SessionPOJO> implements ISessionService {

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactory;

    @Nullable
    @Override
    public String createNewSession(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Login is empty.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Password is empty.");

        @NotNull final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserPOJO user = userRepository.findByLogin(login);
        if (user == null) throw new AccessDeniedException("This User does not exist.");

        @NotNull final String entryPasswordHash = MD5Util.mdHashCode(password);
        if (!user.getPassHash().equals(entryPasswordHash)) throw new AccessDeniedException("Wrong password.");

        @NotNull final SessionPOJO sessionPojo = new SessionPOJO();
        sessionPojo.setUser(user);
        sessionPojo.setRole(user.getRole());
        @Nullable final String signature = SignatureUtil.sign(SessionPOJO.toSession(sessionPojo));
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException("Something go wrong. Try again.");
        sessionPojo.setSignature(signature);

        persist(sessionPojo);
        @Nullable final String encryptedSession = EncryptionUtil.encrypt(SessionPOJO.toSession(sessionPojo));
        entityManager.close();

        return encryptedSession;
    }

    @NotNull
    @Override
    public List<SessionPOJO> findAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final List<SessionPOJO> sessionList = sessionRepository.findAll();
        entityManager.close();
        return sessionList;
    }

    @Nullable
    @Override
    public SessionPOJO findOne(@Nullable final String sessionId) throws Exception {
        if (sessionId == null || sessionId.isEmpty()) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @Nullable final SessionPOJO session = sessionRepository.findOne(sessionId);
        entityManager.close();
        return session;
    }

    @Nullable
    @Override
    public SessionPOJO persist(@Nullable final SessionPOJO session) throws Exception {
        if (session == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            sessionRepository.persist(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionPOJO merge(@Nullable final SessionPOJO session) throws Exception {
        if (session == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            sessionRepository.merge(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String sessionId) throws Exception {
        if (sessionId == null || sessionId.isEmpty()) return;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            sessionRepository.remove(sessionId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            sessionRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isContains(@NotNull final Session session) throws Exception {
        @Nullable final SessionPOJO checkThisSession = findOne(session.getId());
        if (checkThisSession == null) return false;

        final boolean isUserIdEquals = Objects.equals(checkThisSession.getUser().getId(), session.getUserId());
        final boolean isCreateDateEquals = Objects.equals(checkThisSession.getTimestamp(), session.getTimestamp());
        final boolean isSignatureEquals = Objects.equals(checkThisSession.getSignature(), session.getSignature());
        final boolean isRoleEquals = Objects.equals(checkThisSession.getRole(), session.getRole());

        return isUserIdEquals && isCreateDateEquals && isSignatureEquals && isRoleEquals;
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied.");
        @Nullable final String userId = session.getUserId();
        @Nullable final String signature = session.getSignature();
        @Nullable final Role role = session.getRole();
        if (userId == null || userId.isEmpty() || signature == null || signature.isEmpty() || role == null) {
            throw new AccessDeniedException("Access denied.");
        }

        if (!isContains(session)) throw new AccessDeniedException("Access denied.");

        final long timeOfExistence = System.currentTimeMillis() - session.getTimestamp();
        if (timeOfExistence > Constant.SESSION_LIFETIME) {
            remove(session.getId());
            throw new AccessDeniedException("Session is over.");
        }
    }

    public void validateSessionAndRole(@Nullable final Session session, @NotNull final Role role) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied.");
        validate(session);
        if (!role.equals(session.getRole())) throw new AccessDeniedException("Access denied. Role type is not allowed.");
    }

}
