package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.IEntityManagerFactoryService;
import ru.rastorguev.tm.api.service.IUserService;
import ru.rastorguev.tm.entity.UserPOJO;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

@RequiredArgsConstructor
public final class UserService extends AbstractService<UserPOJO> implements IUserService {

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactory;

    @NotNull
    @Override
    public List<UserPOJO> findAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final List<UserPOJO> userList = userRepository.findAll();
        entityManager.close();
        return userList;
    }

    @Nullable
    @Override
    public UserPOJO findOne(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserPOJO user = userRepository.findOne(userId);
        entityManager.close();
        return user;
    }


    @Override
    public UserPOJO persist(@Nullable final UserPOJO user) throws Exception {
        if (user == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.persist(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserPOJO merge(@Nullable final UserPOJO user) throws Exception {
        if (user == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.merge(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.remove(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            userRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserPOJO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserPOJO user = userRepository.findByLogin(login);
        entityManager.close();
        return user;
    }

    @Override
    public boolean isExistByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        boolean isExist = userRepository.isExistByLogin(login);
        entityManager.close();
        return isExist;
    }

    @Override
    public UserPOJO createUser(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final UserPOJO user = new UserPOJO();
        user.setLogin(login);
        user.setPassHash(mdHashCode(password));
        persist(user);
        return user;
    }

    @Override
    public UserPOJO createAdmin(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final UserPOJO admin = new UserPOJO();
        admin.setLogin(login);
        admin.setPassHash(mdHashCode(password));
        admin.setRole(Role.ADMINISTRATOR);
        persist(admin);
        return admin;
    }

    @Override
    public void updateUserLogin(@Nullable final String userId, @Nullable final String login) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Login is empty");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("Login already exist.");
        @Nullable final UserPOJO user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        user.setLogin(login);
        merge(user);
    }

    @Override
    public void updateUserPassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String old
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        if (old == null || old.isEmpty()) throw new AccessDeniedException("Wrong old password. Try again.");
        @Nullable final UserPOJO user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        final boolean isOldPassEquals = user.getPassHash().equals(mdHashCode(old));
        if (!isOldPassEquals) throw new AccessDeniedException("Old password did not match.");
        user.setPassHash(mdHashCode(password));
        merge(user);
    }

    @Override
    public void loadFromDto(@Nullable final List<UserPOJO> userList) throws Exception {
        if (userList == null) return;
        for (final UserPOJO user : userList){
            @Nullable UserPOJO u = findOne(user.getId());
            if (u == null) persist(user);
            else merge(user);
        }
    }

    @NotNull
    @Override
    public Role getUserRole(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        @Nullable final UserPOJO user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        @Nullable final Role userRole = user.getRole();
        return userRole;
    }

}