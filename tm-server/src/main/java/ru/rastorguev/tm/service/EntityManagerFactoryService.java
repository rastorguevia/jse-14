package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.service.IEntityManagerFactoryService;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.entity.ProjectPOJO;
import ru.rastorguev.tm.entity.SessionPOJO;
import ru.rastorguev.tm.entity.TaskPOJO;
import ru.rastorguev.tm.entity.UserPOJO;
import ru.rastorguev.tm.util.PropertyUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class EntityManagerFactoryService implements IEntityManagerFactoryService {

    private EntityManagerFactory entityManagerFactory;

    @NotNull
    public EntityManagerFactory factory() throws Exception {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, PropertyUtil.getDbProperty(Constant.DB_DRIVER));
        settings.put(Environment.URL, PropertyUtil.getDbProperty(Constant.DB_HIBERNATE_URL));
        settings.put(Environment.USER, PropertyUtil.getDbProperty(Constant.DB_USER));
        settings.put(Environment.PASS, PropertyUtil.getDbProperty(Constant.DB_PASSWORD));
        settings.put(Environment.DIALECT, PropertyUtil.getDbProperty(Constant.DB_MYSQL_DIALECT));
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.C3P0_MAX_SIZE, "140");
        settings.put(Environment.C3P0_MIN_SIZE, "10");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(TaskPOJO.class);
        sources.addAnnotatedClass(ProjectPOJO.class);
        sources.addAnnotatedClass(UserPOJO.class);
        sources.addAnnotatedClass(SessionPOJO.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    public EntityManager getEntityManager() throws Exception {
        if (entityManagerFactory == null) {
            entityManagerFactory = factory();
        }
        return entityManagerFactory.createEntityManager();
    }

}
