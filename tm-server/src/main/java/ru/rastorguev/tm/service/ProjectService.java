package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.service.IEntityManagerFactoryService;
import ru.rastorguev.tm.api.service.IProjectService;
import ru.rastorguev.tm.entity.ProjectPOJO;
import ru.rastorguev.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public final class ProjectService extends AbstractService<ProjectPOJO> implements IProjectService {

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactory;

    @NotNull
    @Override
    public List<ProjectPOJO> findAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectPOJO> projectList = projectRepository.findAll();
        entityManager.close();
        return projectList;
    }

    @Nullable
    @Override
    public ProjectPOJO findOne(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final ProjectPOJO project = projectRepository.findOne(projectId);
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public ProjectPOJO persist(@Nullable final ProjectPOJO project) throws Exception {
        if (project == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.persist(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectPOJO merge(@Nullable final ProjectPOJO project) throws Exception {
        if (project == null) return null;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.merge(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.remove(projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectPOJO> projectList = projectRepository.findAllByUserId(userId);
        entityManager.close();
        return projectList;
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAllByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectPOJO> projectList = projectRepository.findAllByUserIdSorted(userId, sortType);
        entityManager.close();
        return projectList;
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public String getProjectIdByNumberForUser(final int number, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final List<ProjectPOJO> filteredListOfProjects = findAllByUserId(userId);
        return filteredListOfProjects.get(number - 1).getId();
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findProjectsByInputAndUserId(@NotNull final String input, @NotNull final String userId) throws Exception {
        if (input ==null || input.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull  final EntityManager entityManager = entityManagerFactory.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectPOJO> projectList = projectRepository.findProjectsByInputAndUserId(input, userId);
        entityManager.close();
        return projectList;
    }

    @Override
    public void loadFromDto(@Nullable final List<ProjectPOJO> projectList) throws Exception {
        if (projectList == null) return;
        for (final ProjectPOJO project : projectList){
            @Nullable ProjectPOJO p = findOne(project.getId());
            if (p == null) persist(project);
            else merge(project);
        }
    }

}