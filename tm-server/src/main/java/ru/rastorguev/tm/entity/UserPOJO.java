package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.dto.Project;
import ru.rastorguev.tm.dto.User;
import ru.rastorguev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "user_table")
public class UserPOJO extends AbstractPOJO {

    @NotNull
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<ProjectPOJO> projects;

    @NotNull
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<TaskPOJO> tasks;

    @NotNull
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<SessionPOJO> sessions;

    @NotNull
    @Column(unique = true)
    private String login = "";

    @NotNull
    private String passHash = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Nullable
    public static User toUser(@Nullable final UserPOJO userPojo) {
        if (userPojo == null) return null;
        @NotNull final User user = new User();
        user.setId(userPojo.getId());
        user.setLogin(userPojo.getLogin());
        user.setPassHash(userPojo.getPassHash());
        user.setRole(userPojo.getRole());
        return user;
    }

}
