package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.dto.Task;
import ru.rastorguev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.dateFormatter;
import static ru.rastorguev.tm.util.DateUtil.stringToDate;

@Getter
@Setter
@Entity
@Table(name = "task_table")
public class TaskPOJO extends AbstractPOJO {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserPOJO user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private ProjectPOJO project;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date startDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    private Long creationDate = System.currentTimeMillis();

    @Nullable
    public static Task toTask(@Nullable final TaskPOJO taskPojo) {
        if (taskPojo == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskPojo.getId());
        task.setUserId(taskPojo.getUser().getId());
        task.setProjectId(taskPojo.getProject().getId());
        task.setProjectName(taskPojo.getProject().getName());
        task.setName(taskPojo.getName());
        task.setDescription(taskPojo.getDescription());
        task.setStartDate(taskPojo.getStartDate());
        task.setEndDate(taskPojo.getEndDate());
        task.setStatus(taskPojo.getStatus());
        task.setCreationDate(taskPojo.getCreationDate());
        return task;
    }

}
