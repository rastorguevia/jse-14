package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.dto.Project;
import ru.rastorguev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static ru.rastorguev.tm.util.DateUtil.dateFormatter;
import static ru.rastorguev.tm.util.DateUtil.stringToDate;

@Getter
@Setter
@Entity
@Table(name = "project_table")
public class ProjectPOJO extends AbstractPOJO {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserPOJO user;

    @NotNull
    @OneToMany(mappedBy = "project", orphanRemoval = true)
    private List<TaskPOJO> tasks;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date startDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    private Long creationDate = System.currentTimeMillis();

    @Nullable
    public static Project toProject(@Nullable final ProjectPOJO projectPojo) {
        if (projectPojo == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectPojo.getId());
        project.setUserId(projectPojo.getUser().getId());
        project.setName(projectPojo.getName());
        project.setDescription(projectPojo.getDescription());
        project.setStartDate(projectPojo.getStartDate());
        project.setEndDate(projectPojo.getEndDate());
        project.setStatus(projectPojo.getStatus());
        project.setCreationDate(projectPojo.getCreationDate());
        return project;
    }

}
