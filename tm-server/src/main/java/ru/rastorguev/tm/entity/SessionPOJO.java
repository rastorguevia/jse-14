package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.dto.Session;
import ru.rastorguev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "session_table")
public class SessionPOJO extends AbstractPOJO {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserPOJO user;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role;

    private long timestamp = new Date().getTime();

    @Nullable
    private  String signature;

    @Nullable
    public static Session toSession(@Nullable final SessionPOJO sessionPojo) {
        if (sessionPojo == null) return null;
        @NotNull final Session session = new Session();
        session.setId(sessionPojo.getId());
        session.setUserId(sessionPojo.getUser().getId());
        session.setRole(sessionPojo.getRole());
        session.setTimestamp(sessionPojo.getTimestamp());
        session.setSignature(sessionPojo.getSignature());
        return session;
    }

}
