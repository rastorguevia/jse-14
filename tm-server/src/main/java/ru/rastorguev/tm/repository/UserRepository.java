package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.entity.UserPOJO;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    public List<UserPOJO> findAll() {
        return entityManager.createQuery("SELECT u FROM UserPOJO u", UserPOJO.class).getResultList();
    }

    @Nullable
    public UserPOJO findOne(@NotNull final String userId) {
        return entityManager.createQuery("SELECT u FROM UserPOJO u WHERE u.id = :id", UserPOJO.class)
                .setParameter("id", userId)
                .getSingleResult();
    }

    public void persist(@NotNull final UserPOJO userPojo) {
        entityManager.persist(userPojo);
    }

    public void merge(@NotNull final UserPOJO userPojo) {
        entityManager.merge(userPojo);
    }

    public void remove(@NotNull final String userId) throws Exception {
        @Nullable final UserPOJO user = findOne(userId);
        if (user == null) throw new Exception("User does not exist.");
        entityManager.remove(user);
    }

    public void removeAll() {
        for (final UserPOJO user : findAll()) entityManager.remove(user);
    }

    @Nullable
    public UserPOJO findByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT u FROM UserPOJO u WHERE u.login = :login", UserPOJO.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    public boolean isExistByLogin(@NotNull final String login) {
        List<UserPOJO> userList = entityManager.createQuery("SELECT u FROM UserPOJO u WHERE u.login = :login", UserPOJO.class)
                .setParameter("login", login)
                .getResultList();
        return !userList.isEmpty();
    }

}
