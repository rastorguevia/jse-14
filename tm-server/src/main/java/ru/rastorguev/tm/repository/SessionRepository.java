package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.entity.SessionPOJO;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    public List<SessionPOJO> findAll() {
        return entityManager.createQuery("SELECT s FROM SessionPOJO s", SessionPOJO.class).getResultList();
    }

    @Nullable
    public SessionPOJO findOne(@NotNull final String sessionId) {
        return entityManager.createQuery("SELECT s FROM SessionPOJO s WHERE s.id = :id", SessionPOJO.class)
                .setParameter("id", sessionId)
                .getSingleResult();
    }

    public void persist(@NotNull final SessionPOJO sessionPojo) {
        entityManager.persist(sessionPojo);
    }

    public void merge(@NotNull final SessionPOJO sessionPojo) {
        entityManager.merge(sessionPojo);
    }

    public void remove(@NotNull final String sessionId) throws Exception {
        @Nullable final SessionPOJO session = findOne(sessionId);
        if (session == null) throw new Exception("Session does not exist.");
        entityManager.remove(session);
    }

    public void removeAll() {
        for (final SessionPOJO session : findAll()) entityManager.remove(session);
    }

}
