package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.TaskPOJO;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public class TaskRepository implements ITaskRepository {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    public List<TaskPOJO> findAll() {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t", TaskPOJO.class).getResultList();
    }

    @Nullable
    public TaskPOJO findOne(@NotNull final String taskId) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.id = :id", TaskPOJO.class)
                .setParameter("id", taskId)
                .getSingleResult();
    }

    public void persist(@NotNull final TaskPOJO taskPojo) {
        entityManager.persist(taskPojo);
    }

    public void merge(@NotNull final TaskPOJO taskPojo) {
        entityManager.merge(taskPojo);
    }

    public void remove(@NotNull final String taskId) throws Exception {
        @Nullable final TaskPOJO task = findOne(taskId);
        if (task == null) throw new Exception("Task does not exist.");
        entityManager.remove(task);
    }

    public void removeAll() {
        for (final TaskPOJO task : findAll()) entityManager.remove(task);
    }

    @NotNull
    public List<TaskPOJO> taskListByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.project.id = :projectId ORDER BY t.creationDate ASC", TaskPOJO.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    public void removeTaskListByProjectId(@NotNull final String projectId) {
        for (final TaskPOJO task : taskListByProjectId(projectId)) entityManager.remove(task);
    }

    @NotNull
    public List<TaskPOJO> taskListByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId ORDER BY t.creationDate ASC", TaskPOJO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    public List<TaskPOJO> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.user.id = :userId " +
                "AND (name LIKE :input OR description LIKE :input)", TaskPOJO.class)
                .setParameter("userId", userId)
                .setParameter("input", "%" + input + "%")
                .getResultList();
    }

    @NotNull
    public List<TaskPOJO> taskListByProjectIdSorted(@NotNull final String projectId, @NotNull final String sortType) {
        return entityManager.createQuery("SELECT t FROM TaskPOJO t WHERE t.project.id = :projectId ORDER BY :sortType", TaskPOJO.class)
                .setParameter("projectId", projectId)
                .setParameter("sortType", sortType)
                .getResultList();
    }

}
