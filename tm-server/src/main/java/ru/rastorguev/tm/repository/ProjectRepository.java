package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.entity.ProjectPOJO;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    public List<ProjectPOJO> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p", ProjectPOJO.class).getResultList();
    }

    @Nullable
    public ProjectPOJO findOne(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.id = :id", ProjectPOJO.class)
                .setParameter("id", projectId)
                .getSingleResult();
    }

    public void persist(@NotNull final ProjectPOJO projectPojo) {
        entityManager.persist(projectPojo);
    }

    public void merge(@NotNull final ProjectPOJO projectPojo) {
        entityManager.merge(projectPojo);
    }

    public void remove(@NotNull final String projectId) throws Exception {
        @Nullable final ProjectPOJO project = findOne(projectId);
        if (project == null) throw new Exception("Project does not exist.");
        entityManager.remove(project);
    }

    public void removeAll() {
        for (final ProjectPOJO project : findAll()) entityManager.remove(project);
    }

    @NotNull
    public List<ProjectPOJO> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId ORDER BY p.creationDate ASC", ProjectPOJO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    public void removeAllByUserId(@NotNull final String userId) {
        for (final ProjectPOJO project : findAllByUserId(userId)) entityManager.remove(project);
    }

    @NotNull
    public List<ProjectPOJO> findAllByUserIdSorted(@NotNull final String userId, @NotNull final String sortType) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId ORDER BY :sortType", ProjectPOJO.class)
                .setParameter("userId", userId)
                .setParameter("sortType", sortType)
                .getResultList();

    }

    @NotNull
    public List<ProjectPOJO> findProjectsByInputAndUserId(@NotNull final String input, @NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId " +
                "AND (name LIKE :input OR description LIKE :input)", ProjectPOJO.class)
                .setParameter("userId", userId)
                .setParameter("input", "%" + input + "%")
                .getResultList();
    }

}
