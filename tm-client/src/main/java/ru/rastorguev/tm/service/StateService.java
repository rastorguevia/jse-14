package ru.rastorguev.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.IStateService;
import ru.rastorguev.tm.endpoint.Role;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor
public final class StateService implements IStateService {

    @Getter
    @Setter
    @Nullable
    private String token;

    @Getter
    @Setter
    @Nullable
    private Role userRole;

    @Override
    public boolean isAuth() {
        return token != null;
    }

    @Override
    public boolean isRolesAllowed(@Nullable final Role... roles) {
        if (roles == null) return false;
        if (token == null || userRole == null) return false;
        @Nullable final List<Role> listOfRoles = Arrays.asList(roles);
        return listOfRoles.contains(userRole);
    }

}
